#!/bin/sh

[ -f .image.name ] || echo docker-from-docker > .image-name

IMAGE_NAME=$(cat .image.name)

WORKSPACE_ROOT="$PWD"

docker container run -it --rm --mount source=/var/run/docker.sock,target=/var/run/docker.sock,type=bind \
    --env WORKSPACE_ROOT="$WORKSPACE_ROOT" --mount source="$WORKSPACE_ROOT",target=/home/dev/dev,type=bind $IMAGE_NAME "$@"
